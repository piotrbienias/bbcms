'use strict';


module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('bb_user_roles', {
            slug: {
                primaryKey: true,
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('bb_user_roles');
    }
};
