'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('bb_user_roles', [{
            name: 'Administrator',
            slug: 'administrator'
        }, {
            name: 'Redaktor',
            slug: 'redaktor'
        }]);
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('bb_user_roles', null, {});
    }
};
