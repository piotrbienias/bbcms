const expect            = require('chai').expect;
const config            = require('../config');
const baseURL           = '/api/models/users';

const agent             = config.request.agent(config.app);


describe('User unit tests', () => {

    before(function(done){
        this.timeout(0);

        config.sync().then(() => {
            done();
        });
    });

    it('POST /api/auth/login', done => {

        let loginData = {
            email: 'admin@example.com',
            password: 'password'
        };

        agent
            .post(config.authURL)
            .send(loginData)
            .end((err, res) => {
                if ( err ) throw err;

                expect(res.body.data).to.be.an('object');
                expect(res.body.data.email).to.equal(loginData.email);

                done();
            });

    });

    it('GET /api/models/users', done => {

        agent
            .get(baseURL)
            .end((err, res) => {
                if ( err ) throw err;

                expect(res.body.data).to.be.an('array');
                expect(res.body.data).to.have.lengthOf(2);

                done();
            });

    });

});