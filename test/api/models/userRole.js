const expect    = require('chai').expect;
const config    = require('./../config');
const baseURL   = '/api/models/user-roles';

const agent     = config.request.agent(config.app);



describe('UserRole unit model tests', function(){

    before(function(done){
        // Disable task timeout so it finishes the sync process
        this.timeout(0);
        
        config.sync().then(() => {
            done();
        }).catch(e => {
            console.log(e);
        });
    });

    it('POST /api/auth/login', done => {

        let authData = {
            email: 'admin@example.com',
            password: 'password'
        };

        agent
            .post(config.authURL)
            .send(authData)
            .end((err, res) => {
                if ( err ) throw err;

                done();
            })

    })

    it('GET /api/models/user-roles', done => {

        agent
            .get(baseURL)
            .end((err, res) => {
                if ( err ) throw err;

                expect(res.body.data).to.have.lengthOf(2);

                done();
            });
        
    });

    it('GET /api/models/user-roles/:slug', done => {

        agent
            .get(baseURL + '/administrator')
            .end((err, res) => {
                if ( err ) throw err;

                expect(res.body.data.slug).to.equal('administrator');

                done();
            });

    });

});