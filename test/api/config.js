const request       = require('supertest');
const path          = require('path');
const app           = require(path.resolve(process.cwd(), 'dist', 'api')).default;
const sync          = require('./sync');
const authURL       = '/api/auth/login';
const agent         = request.agent(app);


module.exports = {
    agent:      agent,
    authURL:    authURL,
    sync:       sync,
    app:        app,
    request:    request
};