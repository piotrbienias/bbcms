const request           = require('supertest');
const expect            = require('chai').expect;
const path              = require('path');
const app               = require(path.resolve(process.cwd(), 'dist', 'api')).default;
const sync              = require('./../sync');
const baseURL           = '/api/auth';


describe('Auth tests', () => {

    before(function(done) {
        this.timeout(0);

        sync().then(() => {
            done();
        });
    });

    it('POST /login - success', done => {

        const data = {
            email: 'admin@example.com',
            password: 'password'
        };

        request(app)
            .post(baseURL + '/login')
            .send(data)
            .set('Accept', 'application/json')
            .end((err, response) => {
                if ( err ) throw err;

                expect(response.body.data).to.be.an('object');
                expect(response.body.data.email).to.equal(data.email);

                done();
            });

    });

    it('POST /login - fail, wrong password', done => {

        const data = {
            email: 'admin@example.com',
            password: 'WrongPassword'
        };

        request(app)
            .post(baseURL + '/login')
            .send(data)
            .end((err, response) => {
                if ( err ) throw err;

                expect(response.body.status).to.equal(401);

                done();
            });

    });

});