const path          = require('path');
const db            = require(path.resolve(process.cwd(), 'dist', 'api', 'models'));
const { execSync }  = require('child_process');


function syncDatabase() {

    return db.sequelize.sync({ force: true }).then(() => {
        // Seed database with data
        execSync('NODE_ENV=test ./node_modules/.bin/sequelize db:seed:all');
    });
}


module.exports = syncDatabase;