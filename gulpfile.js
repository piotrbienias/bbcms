var gulp        = require('gulp'),
    babel       = require('gulp-babel'),
    nodemon     = require('gulp-nodemon');

var series      = gulp.series,
    task        = gulp.task;



function compileTask(cb) {
    return gulp.src('src/api/**/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest('dist/api'));
}

function nodemonTask(cb) {
    nodemon({
        script: 'dist/api/index.js',
        ignore: 'node_modules/',
        tasks: ['compile'],
        watch: 'src/api/**/*.js'
    }).on('restart', function(){
        console.log('Node.js restarted');
    });

    cb();
}

task('compile', compileTask);
task('nodemon', nodemonTask);


exports.default = series( 'compile', 'nodemon' );