FROM node:10.15.1-alpine

WORKDIR /opt/bbcms

COPY package.json .

RUN npm install --quiet -g gulp && \
    npm install --quiet