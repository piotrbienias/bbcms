const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('html-webpack-plugin');


module.exports = {
    devtool: 'inline-source-map',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist', 'dashboard')
    },
    devServer: {
        host: '0.0.0.0',
        port: 3000,
        hot: true,
        filename: 'main.js'
    },
    entry: './src/dashboard/index.js',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['dist/dashboard']),
        new HtmlWebpackPlugin({
            template: './src/dashboard/index.html'
        })
    ]
};