// Require dotenv and load env variables
require('dotenv').config();


module.exports = {
    development: {
        username: 'bbcms',
        password: process.env.POSTGRES_PASSWORD,
        database: 'bbcms',
        host: 'postgres',
        dialect: 'postgres'
    },
    test: {
        username: 'bbcms',
        password: process.env.POSTGRES_PASSWORD,
        database: 'bbcms',
        host: 'postgres',
        dialect: 'postgres',
        logging: false
    },
    production: {
        username: 'bbcms',
        password: process.env.POSTGRES_PASSWORD,
        database: 'bbcms',
        host: 'localhost',
        dialect: 'postgres'
    }
};