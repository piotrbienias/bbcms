module.exports = {
    apps: [{
        name:   'bbcms',
        script: 'npm',
        args:   'start',
        env: {
            'NODE_ENV': 'production'
        }
    }]
};