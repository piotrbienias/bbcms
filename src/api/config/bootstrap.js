// Project bootstrap file
import Dotenv from 'dotenv';

// Load environment variables from .env file
Dotenv.config();