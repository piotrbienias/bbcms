'use strict';

import { Router } from 'express';

import models from '../../models';
import BBCmsResponse from '../../lib/response';
import { createToken, verifyToken } from '../../lib/auth';
import { AUTH_COOKIE_NAME } from '../../config/constants';


const router = Router();


/**
 * Login route. First thing is to verify given password for specific e-mail address.
 * If verification went wrong, return 401 Unauthorized.
 * Otherwise, if password was verified, check for authorization cookie. If it is set,
 * verify it and return success message. In other case, if cookie is invalid,
 * return 401 Unauthorized. If cookie is not present (user is not logged in),
 * create new token and save it in HttpOnly cookie.
 */
router.post('/login', (req, res) => {
    models.User.verifyPassword(req.body).then(user => {
        if ( user ) {
            let userData = user.serialize(),
                token;

            if ( req.cookies[ AUTH_COOKIE_NAME ] && verifyToken( req.cookies[ AUTH_COOKIE_NAME ] ) ) {
                token = req.cookies[ AUTH_COOKIE_NAME ];
            } else {
                // otherwise create new one
                token = createToken( { id: user.get('id') } );
            }

            // token expiration time in miliseconds (3600 * 1000 = 1 hour)
            let tokenExpirationTime = parseInt( process.env.AUTH_TOKEN_EXPIRATION ) * 1000;

            // set HttpOnly authorization cookie with token value
            res.cookie(
                AUTH_COOKIE_NAME,
                token,
                {
                    expires: new Date(Date.now() + tokenExpirationTime),
                    httpOnly: true
                }
            );

            // login is OK
            res.send(new BBCmsResponse(200, userData, 'Successful login'));
        } else {
            res.status(401).send(new BBCmsResponse(401, null, 'Wrong e-mail or password'));
        }
    }).catch(e => {
        console.log(e);
        // if error ocurred, it means that token was invalid (error by verifyToken method)
        res.status(401).send(new BBCmsResponse(400, null, 'Invalid authorization token'));
    });
});


export default router;