'use strict';

// Combine all models' routers into one and export it
import { Router } from 'express';

import userRolesRouter      from './userRole';
import usersRouter          from './user';

const router = Router();


router.use('/user-roles',   userRolesRouter);
router.use('/users',        usersRouter);


export default router;