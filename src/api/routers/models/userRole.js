'use strict';

// User roles router 
import { Router } from 'express';
import models from '../../models';
import BBCmsResponse from '../../lib/response';

const router = Router();


// Get all user roles
router.get('/', (req, res) => {
    models.UserRole.findAll().then(userRoles => {
        let mappedUserRoles = userRoles.map(userRole => {
            return userRole.toJSON();
        });

        res.status(200).send(new BBCmsResponse(200, mappedUserRoles));
    });
});


// Get single user role by it's primary key
router.get('/:slug', (req, res) => {
    models.UserRole.findByPk(req.params['slug']).then(userRole => {
        if ( userRole ) {
            res.send(new BBCmsResponse(200, userRole.toJSON()));
        } else {
            res.status(404).send(new BBCmsResponse(404, null, 'User role does not exist'));
        }
    });
});


export default router;