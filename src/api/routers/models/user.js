'use strict';

import { Router } from 'express';
import models from '../../models';
import BBCmsResponse from '../../lib/response';

const router = Router();


// get all users
router.get('/', (req, res) => {
    models.User.scope(['withUserRole']).findAll().then(users => {
        let mappedUsers = users.map(user => {
            return user.serialize();
        });

        res.send(new BBCmsResponse(200, mappedUsers));
    });
});


export default router;