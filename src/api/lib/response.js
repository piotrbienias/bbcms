'use strict';


class BBCmsResponse {

    constructor(status, data, message = null) {
        this.status = status;
        this.data = data;
        this.message = message;

        return this.getResponseData();
    }

    getResponseData() {
        let responseObject = {
            status: this.status,
            data: this.data
        };

        if ( this.message && this.message !== null ) responseObject['message'] = this.message;

        return responseObject;
    }

    static unauthorized() {
        return {
            status: 401,
            data: null,
            message: 'Unauthorized'
        };
    }

}


export default BBCmsResponse;