'use strict';

import models from '../models';
import { verifyToken } from './auth';
import { AUTH_COOKIE_NAME } from '../config/constants';
import BBCmsResponse from './response';


/**
 * Authorization middleware - verifies by `bbcms-auth-token`
 * 
 * @param {Object} req HTTP Request
 * @param {Object} res HTTP Response
 * @param {Function} next Callback method
 */
export const authorization = (req, res, next) => {

    if ( req.method === 'OPTIONS' || process.env.NODE_ENV === 'development' || req.originalUrl === '/api/auth/login' ) {
        next()
    } else {
        const authorizationToken = req.cookies[ AUTH_COOKIE_NAME ];

        try {
            const tokenData = verifyToken(authorizationToken);

            // TODO: check if user account is active, otherwise return 401
            models.User.findByPk(tokenData.id).then(user => {
                if ( user ) {
                    req.user = user.serialize();
                    next();
                } else {
                    throw new Error();
                }
            });
        } catch (e) {
            res.clearCookie(AUTH_COOKIE_NAME, { httpOnly: true });
            res.status(401).send(BBCmsResponse.unauthorized());
        }
    }

}