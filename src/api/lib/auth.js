'use strict';

import jwt from 'jsonwebtoken';


/**
 * Create new token with encoded data.
 * 
 * @param {Object} data Data to be encoded in token
 */
export const createToken = data => {
    
    return jwt.sign(
        data,
        process.env.AUTH_SECRET_KEY,
        {
            expiresIn: process.env.AUTH_TOKEN_EXPIRATION
        }
    );

};


/**
 * Verify given authorization token agains project Secret Key.
 * 
 * @param {String} token Authorization token
 */
export const verifyToken = token => {

    return jwt.verify(
        token,
        process.env.AUTH_SECRET_KEY
    );

};