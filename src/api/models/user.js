'use strict';

import bcrypt from 'bcryptjs';


export default (sequelize, DataTypes) => {

    const User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            unique: true,
            autoIncrement: true
        },
        email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        name: DataTypes.STRING,
        lastName: DataTypes.STRING,
        description: DataTypes.TEXT,
        photoUrl: DataTypes.STRING,
        userRoleSlug: {
            type: DataTypes.STRING,
            references: {
                model: sequelize.models.UserRole,
                key: 'slug'
            },
            allowNull: false
        },
        isActive: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    }, {
        tableName: 'bb_users',
        timestamps: true,
        paranoid: true,
        scopes: {
            withUserRole: function() {
                return {
                    include: {
                        model: sequelize.models.UserRole
                    }
                };
            }
        }
    });

    User.associate = function(models) {
        User.belongsTo(models.UserRole, { foreignKey: 'userRoleSlug' });
    }

    User.verifyPassword = function(data) {
        return this.findOne({ where: { email: data['email'] } }).then(user => {
            if ( user ) {
                const verification = bcrypt.compareSync(data['password'], user.get('password'));
                return verification ? user : null;
            }
        });
    }

    User.prototype.serialize = function() {
        let user = {
            id:             this.get('id'),
            email:          this.get('email'),
            name:           this.get('name'),
            lastName:       this.get('lastName'),
            description:    this.get('description'),
            photoUrl:       this.get('photoUrl'),
            isActive:       this.get('isActive')
        };

        user.userRole = this.UserRole ? this.UserRole.toJSON() : this.get('userRoleSlug');

        return user;
    }

    return User;

};