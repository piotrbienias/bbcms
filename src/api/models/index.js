'use strict';

import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';

const configObject = require(path.resolve(process.cwd(), 'config', 'database.js'));

const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
let config = configObject[env];
const db = {};


let sequelize;

// http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
config['operatorsAliases'] = false;

if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
    sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        const model = sequelize['import'](path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});


db.sequelize = sequelize;
db.Sequelize = Sequelize;


module.exports = db;