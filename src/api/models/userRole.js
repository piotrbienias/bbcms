'use strict';


export default (sequelize, DataTypes) => {

    const UserRole = sequelize.define('UserRole', {
        slug: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        }
    }, {
        tableName: 'bb_user_roles',
        timestamps: false,
        paranoid: false
    });

    return UserRole;
    
};