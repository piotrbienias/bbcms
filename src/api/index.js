'use strict';

// Load bootstrap file
// Must be done before any other configuration
import './config/bootstrap';

import express from 'express';
import cookieParser from 'cookie-parser';

import authRouter   from './routers/auth';
import modelsRouter from './routers/models';

import { authorization } from './lib/middlewares';


const app = express();

// Static files from dashboard App
app.use(express.static('dist/dashboard'));


// Global middlewares
app.use(express.json());
app.use(cookieParser());
app.use(authorization);


// API Routers
app.use('/api/auth',    authRouter);
app.use('/api/models/', modelsRouter);


// Start the app at given PORT
app.listen(process.env.PORT || 8080, () => {
    console.log(`Express listening on port ${process.env.PORT || 8080}`);
});


export default app;